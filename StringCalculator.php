<?php 

class StringCalculator 
{
	const DEFAULT_DELIMITER = ",";

	public function add($string)
	{
		if($string === "") {
			return 0;
		}

		// Si empieza por "//"
		if(preg_match('/^\/\//', $string)) {			
			preg_match('/^\/\/(.?)\\n/', $string, $delimiter);				
			$delimiter = $delimiter[1];				
			$string = preg_replace("/\/\/".preg_quote($delimiter)."\\n/", '', $string);						
			$string = str_replace($delimiter, self::DEFAULT_DELIMITER, $string);						

		} else {

			$string = str_replace("\n", self::DEFAULT_DELIMITER, $string);			
		}

		$numbers = explode(self::DEFAULT_DELIMITER, $string);

		return array_sum($numbers);
	}	

}
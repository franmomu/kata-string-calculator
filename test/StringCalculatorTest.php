<?php

require "StringCalculator.php";

class StringCalculatorTest extends PHPUnit_Framework_TestCase
{
	public function testAddEmpty()
	{
		$stringCalculator = new StringCalculator();
		$this->assertTrue($stringCalculator->add("") === 0);
	}

	public function testAddOne()
	{
		$stringCalculator = new StringCalculator();
		$this->assertTrue($stringCalculator->add("1") === 1);
		$this->assertTrue($stringCalculator->add("2") === 2);
		$this->assertTrue($stringCalculator->add("12") === 12);
	}

	public function testAddTwo()
	{
		$stringCalculator = new StringCalculator();
		$this->assertTrue($stringCalculator->add("1,2") === 3);
		$this->assertTrue($stringCalculator->add("2,4") === 6);
		$this->assertTrue($stringCalculator->add("12,20") === 32);
	}

	public function testAddSeveral()
	{
		$stringCalculator = new StringCalculator();
		$this->assertTrue($stringCalculator->add("1,2,3") === 6);
		$this->assertTrue($stringCalculator->add("2,4,1,1,1,1") === 10);
		$this->assertTrue($stringCalculator->add("12,20,200,1") === 233);
	}

	public function testAddLineBreak()
	{
		$stringCalculator = new StringCalculator();
		$this->assertTrue($stringCalculator->add("1\n2,3") === 6);
		$this->assertTrue($stringCalculator->add("2,4,1\n1,1\n1") === 10);
		$this->assertTrue($stringCalculator->add("12\n20\n200\n1") === 233);	
	}

	public function testAddDelimiter()
	{
		$stringCalculator = new StringCalculator();
		$this->assertTrue($stringCalculator->add("//;\n1;2") === 3);
		$this->assertTrue($stringCalculator->add("//.\n2.4.1.1.1.1") === 10);
		$this->assertTrue($stringCalculator->add("//-\n12-20-200-1") === 233);	
		$this->assertTrue($stringCalculator->add("//p\n12p20p200p1") === 233);	
		$this->assertTrue($stringCalculator->add("//[\n12[20[200[1") === 233);	
		$this->assertTrue($stringCalculator->add("//:\n12:20:200:1") === 233);	
	}
}